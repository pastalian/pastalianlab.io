---
title: "GPG鍵生成"
date: 2020-03-12T16:31:27+09:00
description: GPG鍵をちゃんと作るようにした
tags:
- encryption
categories:
- software
---

今までGPG鍵は自分用だったのでデフォルト設定で使用していたが、公開鍵を渡す機会があったのでちゃんと生成してみた。

## 前置き
鍵をMasterとSubに分ける。
- Master
    自他の鍵の署名（Certify）のみに使用。安全なデバイス一台に保管する。
- Sub
    署名、暗号化、認証に使用する。必要なデバイスで共有する。

暗号アルゴリズムは互換性を考えずにcv25519を使用する。

## 鍵生成
### Master
Certifyのみ有効化し、有効期限を無くす。

```sh
gpg --expert --full-gen-key

Please select what kind of key you want:
  (11) ECC (set your own capabilities)
Your selection? 11

Possible actions for a ECDSA/EdDSA key: Sign Certify Authenticate
Current allowed actions: Sign Certify

   (S) Toggle the sign capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? s

Possible actions for a ECDSA/EdDSA key: Sign Certify Authenticate
Current allowed actions: Certify

   (S) Toggle the sign capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? q
Please select which elliptic curve you want:
   (1) Curve 25519
Your selection? 1
Please specify how long the key should be valid.
         0 = key does not expire
Key is valid for? (0) 0
Key does not expire at all
Is this correct? (y/N) y

GnuPG needs to construct a user ID to identify your key.

Real name: pastalian
Email address: example@example.com
Comment:
You selected this USER-ID:
    "pastalian <example@example.com>"

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? o
public and secret key created and signed.

pub   ed25519 2020-03-12 [C]
      BAC7AE53C7B6A8BEBEFBE7A31A93FDD1E5D61980
uid                      pastalian <example@example.com>
```

### Sub
署名、暗号化、認証それぞれの鍵を生成する。有効期限は1年にする。

```sh
gpg --expert --edit-key <USER-ID>

gpg> addkey
Please select what kind of key you want:
  (10) ECC (sign only)
Your selection? 10
Please select which elliptic curve you want:
   (1) Curve 25519
Your selection? 1
Please specify how long the key should be valid.
      <n>y = key expires in n years
Key is valid for? (0) 1y
Key expires at Fri 12 Mar 2021 05:01:28 PM JST
Is this correct? (y/N) y
Really create? (y/N) y

sec  ed25519/1A93FDD1E5D61980
     created: 2020-03-12  expires: never       usage: C
     trust: ultimate      validity: ultimate
ssb  ed25519/572352367F8E442C
     created: 2020-03-12  expires: 2021-03-12  usage: S
[ultimate] (1). pastalian <example@example.com>

gpg> addkey
Please select what kind of key you want:
  (12) ECC (encrypt only)
Your selection? 12
Please select which elliptic curve you want:
   (1) Curve 25519
Your selection? 1
Please specify how long the key should be valid.
      <n>y = key expires in n years
Key is valid for? (0) 1y
Key expires at Fri 12 Mar 2021 05:01:55 PM JST
Is this correct? (y/N) y
Really create? (y/N) y

sec  ed25519/1A93FDD1E5D61980
     created: 2020-03-12  expires: never       usage: C
     trust: ultimate      validity: ultimate
ssb  ed25519/572352367F8E442C
     created: 2020-03-12  expires: 2021-03-12  usage: S
ssb  cv25519/41F29DB31C2180AD
     created: 2020-03-12  expires: 2021-03-12  usage: E
[ultimate] (1). pastalian <example@example.com>

gpg> addkey
Please select what kind of key you want:
  (11) ECC (set your own capabilities)
Your selection? 11

Possible actions for a ECDSA/EdDSA key: Sign Authenticate
Current allowed actions: Sign

   (S) Toggle the sign capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? s

Possible actions for a ECDSA/EdDSA key: Sign Authenticate
Current allowed actions:

   (S) Toggle the sign capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? a

Possible actions for a ECDSA/EdDSA key: Sign Authenticate
Current allowed actions: Authenticate

   (S) Toggle the sign capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? q
Please select which elliptic curve you want:
   (1) Curve 25519
Your selection? 1
Please specify how long the key should be valid.
      <n>y = key expires in n years
Key is valid for? (0) 1y
Key expires at Fri 12 Mar 2021 05:02:09 PM JST
Is this correct? (y/N) y
Really create? (y/N) y

sec  ed25519/1A93FDD1E5D61980
     created: 2020-03-12  expires: never       usage: C
     trust: ultimate      validity: ultimate
ssb  ed25519/572352367F8E442C
     created: 2020-03-12  expires: 2021-03-12  usage: S
ssb  cv25519/41F29DB31C2180AD
     created: 2020-03-12  expires: 2021-03-12  usage: E
ssb  ed25519/3C6296AFA468FCAB
     created: 2020-03-12  expires: 2021-03-12  usage: A
[ultimate] (1). pastalian <example@example.com>

gpg> q
Save changes? (y/N) y
```

## 鍵の使い回し
SubのパスフレーズをMasterとは違うものに設定してから複数デバイスに使い回す。

```sh
gpg --export-secret-subkeys <USER-ID> > /tmp/subkey.key
mkdir /tmp/gpg
gpg --homedir /tmp/gpg --import /tmp/subkey.key
gpg --homedir /tmp/gpg --edit-key <USER-ID>
gpg> passwd
gpg> q
gpg --homedir /tmp/gpg --export-secret-subkeys <USER-ID> > subkey_newpass.key
```

生成された`subkey_newpass.gpg`をそれぞれの端末でインポートする。
自分の鍵なのでtrustもしておく。
```sh
gpg --import subkey_newpass.key
gpg --edit-key <USER-ID>
gpg> trust
Please decide how far you trust this user to correctly verify other users' keys
(by looking at passports, checking fingerprints from different sources, etc.)

  5 = I trust ultimately

Your decision? 5
Do you really want to set this key to ultimate trust? (y/N) y
```

## 有効期限延長
Subの有効期限が切れたらMasterを使って延長する。
パスフレーズの変更も再度行う（略）。

```sh
gpg --edit-key <USER-ID>

Secret key is available.

sec  ed25519/1A93FDD1E5D61980
     created: 2020-03-12  expires: never       usage: C
     trust: ultimate      validity: ultimate
ssb  ed25519/572352367F8E442C
     created: 2020-03-12  expires: 2021-03-12  usage: S
ssb  cv25519/41F29DB31C2180AD
     created: 2020-03-12  expires: 2021-03-12  usage: E
ssb  ed25519/3C6296AFA468FCAB
     created: 2020-03-12  expires: 2021-03-12  usage: A
[ultimate] (1). pastalian <example@example.com>

gpg> key 1
gpg> key 2
gpg> key 3
gpg> expire
Are you sure you want to change the expiration time for multiple subkeys? (y/N) y
Please specify how long the key should be valid.
      <n>y = key expires in n years
Key is valid for? (0) 1y
Key expires at Fri 12 Mar 2021 05:03:09 PM JST
Is this correct? (y/N) y

sec  ed25519/1A93FDD1E5D61980
     created: 2020-03-12  expires: never       usage: C
     trust: ultimate      validity: ultimate
ssb  ed25519/572352367F8E442C
     created: 2020-03-12  expires: 2021-03-12  usage: S
ssb  cv25519/41F29DB31C2180AD
     created: 2020-03-12  expires: 2021-03-12  usage: E
ssb  ed25519/3C6296AFA468FCAB
     created: 2020-03-12  expires: 2021-03-12  usage: A
[ultimate] (1). pastalian <example@example.com>

gpg> q
Save changes? (y/N) y
```

## 参考
[ArchWiki](https://wiki.archlinux.jp/index.php/GnuPG)
[Gentoo Infrastructure's key generation instructions](https://wiki.gentoo.org/wiki/Project:Infrastructure/Generating_GLEP_63_based_OpenPGP_keys#How_to_generate_the_GLEP_63-compliant_OpenPGP_key)
