---
title: "Install Osu with Wine"
date: 2019-09-17
description: Linuxにwineでosuをインストールする手順
tags:
- game
- gentoo
categories:
- software
---

Gentooにwineでosuをインストールする手順

テスト環境

- wine-staging-4.1.6
- bspwm
- nvidia
- windowsからフォントを拝借済み

winetricksをインストール
```sh
curl -OL https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
chmod +x winetricks
mv winetricks ~/bin/
```

> レポジトリにwinetricksはあるけれど依存関係が気に入らないのでバイナリを入れた。この場合実行時に必要なパッケージを自分で管理する必要があるので注意。(like app-arch/cabextract)

prefix作成
```sh
WINEPREFIX=~/.wine-osu WINEARCH=win32 winetricks dotnet462 gdiplus
```

ゴミが大量に生成されるので掃除
```sh
rm -f ~/.local/share/applications/wine-extension*.desktop
rm -f ~/.local/share/icons/hicolor/*/*/application-x-wine-extension*
rm -rf ~/.local/share/mime
update-desktop-database ~/.local/share/applications
```

ゴミ生成の停止
```sh
cat > associations.reg << "EOF"
Windows Registry Editor Version 5.00
[HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\RunServices]
"winemenubuilder"="C:\\windows\\system32\\winemenubuilder.exe -r"
EOF

WINEPREFIX=~/.wine-osu wine regedit associations.reg
```

フォントヒンティングの修正
```sh
WINEPREFIX=~/.wine-osu winetricks settings fontsmooth=rgb
```

サウンドドライバをalsaに
```sh
WINEPREFIX=~/.wine-osu winetricks sound=alsa
```

音の遅延対策
```sh
cat > dsound.reg << "EOF"
Windows Registry Editor Version 5.00
[HKEY_CURRENT_USER\Software\Wine\DirectSound]
"HelBuflen"="512"
"SndQueueMax"="3"
EOF

WINEPREFIX=~/.wine-osu wine regedit dsound.reg
```

osu installer
```sh
curl -O 'https://m1.ppy.sh/r/osu!install.exe'
WINEPREFIX=~/.wine-osu wine osu\!install.exe
```
> tiling wmだとフルスクリーン中にワークスペースを切り替えるとフリーズするので注意。ボーダーレス推奨

desktopエントリ作成
```sh
cat > ~/.local/share/applications/osu.desktop << "EOF"
[Desktop Entry]
Type=Application
Name=osu!
Exec=env vblank_mode=0 WINEPREFIX=/home/USER/.wine-osu /usr/bin/wine /home/USER/.wine-osu/drive_c/osu/osu\!.exe
StartupWMClass=osu!.exe
EOF
```
> USERを自身のユーザ名に変更

終わり
