+++
title = "LSTM Regression for NLP"
date = "2019-11-13"
tags = ["nlp", "python"]
categories = ["dev"]
description = "LSTMで自然言語の回帰を行う"
+++

LSTMで自然言語処理の回帰を行う。

課題として[Codeforces](https://codeforces.com/)の問題文から難易度を推定することを設定する。
Codeforcesではそれぞれの問題に難易度が設定されているので簡単にデータセットの作成ができた。  
また、難易度と同様にタグ（問題・解法の種類）も設定されているので別に分類も行ったが、ほぼ同じなので省略。

# Prepare dataset
- データファイル: problems.csv
    - 問題文: main_text
    - 難易度: difficulty


## ロード
```python
df = pd.read_csv('problems.csv')
df = df.dropna(subset=['difficulty'])
x, y = df['main_text'].values, df['difficulty'].values.reshape(-1, 1)
```

## 標準化
標準化せずに試したら予測難易度がほぼ定数になった。大事。
```python
stdsc = StandardScaler().fit(y)
y = stdsc.transform(y)
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, 
                                                     random_state=93187)
```

# Tokenize
単語数は適当。溢れた単語は頻度の低いものから消えると思われる。問題文の長さは超バラバラなのでこれも適当。
## 
```python
max_features = 20000
tokenizer = Tokenizer(num_words=max_features)
tokenizer.fit_on_texts(x_train)
x2_train = tokenizer.texts_to_sequences(x_train)
x2_test = tokenizer.texts_to_sequences(x_test)

MAX_LEN = 512
x3_train = sequence.pad_sequences(x2_train, maxlen=MAX_LEN)
x3_test = sequence.pad_sequences(x2_test, maxlen=MAX_LEN)
```

# Model
いくつかメモ

- 埋め込みベクトルのサイズを32,128で試したが差がわからなかった（有意な差の検証方法を知らない）。
- LSTMの次元数も同様
- 双方向LSTMは学習時間が超長いわりに差が（同様）
- emb層のマスクも...
- ドロップアウトをしない場合明らかに過学習の影響が強い

```python
embed_size = 128
output_dimention = int(embed_size * (2 / 3))
x = Embedding(max_features, embed_size, mask_zero=True)(inp)
x = LSTM(output_dimention, return_sequences=True, dropout=0.2, recurrent_dropout=0.2)(x)
#x = Bidirectional(LSTM(output_dimention, return_sequences=True, dropout=0.2, recurrent_dropout=0.2))(x)
x = MaskedGlobalMaxPool1D()(x)
#x = GlobalMaxPool1D()(x)
x = Dense(1)(x)

inp = Input(shape=(MAX_LEN, ))
```

```python
model = Model(inputs=inp, outputs=x)
model.compile(loss='mean_squared_error',
              optimizer='adam',
              metrics=['mae'])
```

## summary
```
Model: "model_1"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
input_1 (InputLayer)         (None, 512)               0         
_________________________________________________________________
embedding_1 (Embedding)      (None, 512, 128)          2560000   
_________________________________________________________________
lstm_1 (LSTM)                (None, 512, 85)           72760     
_________________________________________________________________
masked_global_max_pool1d_1 ( (None, 85)                0         
_________________________________________________________________
dense_1 (Dense)              (None, 1)                 86        
=================================================================
Total params: 2,632,846
Trainable params: 2,632,846
Non-trainable params: 0
_________________________________________________________________
```

# Train
```python
batch_size = 64
epochs = 3
model.fit(x3_train, y_train, batch_size=batch_size, epochs=epochs,
          validation_split=0.1)
```

```
Train on 3608 samples, validate on 401 samples
Epoch 1/3
3608/3608 [==============================] - 94s 26ms/step - loss: 0.9786 - mean_absolute_error: 0.8366 - val_loss: 0.9318 - val_mean_absolute_error: 0.8202
Epoch 2/3
3608/3608 [==============================] - 93s 26ms/step - loss: 0.8152 - mean_absolute_error: 0.7510 - val_loss: 0.8027 - val_mean_absolute_error: 0.7448
Epoch 3/3
3608/3608 [==============================] - 93s 26ms/step - loss: 0.5767 - mean_absolute_error: 0.6083 - val_loss: 0.8151 - val_mean_absolute_error: 0.7368
```

# Eval
```python
scores = model.evaluate(x3_test, y_test)
print('mse:', scores[0])
print('mae:', scores[1])
```

```
1003/1003 [==============================] - 16s 16ms/step
mse: 0.8467014510990497
mae: 0.7546261067048146
```

# Predict
```python
y_pred = model.predict(x3_test)
pred = stdsc.inverse_transform(y_pred).flatten()
t_val = stdsc.inverse_transform(y_test).flatten()

compare = pd.DataFrame(np.array([t_val, pred]).T)
compare.columns = ['True', 'Predict']

compare.head()
```

```
	True	Predict
0	2000.0	2025.838867
1	800.0	1681.901611
2	1300.0	1336.252441
3	2200.0	1828.409180
4	2300.0	1680.156860
```

# Conclusion
当たり前のように精度が酷い。パラメータ等ほぼ適当なので調整すれば多少良くなる気はする。  
今回はバックの知識無しでとりあえず実装した感じなのでここまでで満足してしまった。
