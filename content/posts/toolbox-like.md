---
title: "Toolbox Like"
date: 2020-11-05T00:25:13+09:00
description: Non systemdでもtoolboxを使いたい
tags:
- container
categories:
- dev
---

[前回](/posts/fedora-toolbox)のToolboxをやはりメインマシン(OpenRC)でも使いたかったので、スクリプト部分だけを貰ってきて個人的に満足なレベルのコンテナを作れるようにした。

## Create volume
流石に違うdistroのホームディレクトリを共有したくないので専用のものを作る。
```sh
podman volume create dev
```

## Create container
```sh
podman create \
    --hostname fedora \
    --ipc host \
    --name fedora \
    --network host \
    --pid host \
    --privileged \
    --security-opt label=disable \
    --ulimit host \
    --userns=keep-id \
    --user root:root \
    --volume dev:/home/$USER \
    --volume /run/dbus/system_bus_socket:/run/dbus/system_bus_socket \
    --volume /dev:/dev:rslave \
    --volume /tmp/.X11-unix:/tmp/.X11-unix \
    -e DISPLAY=$DISPLAY \
    -it \
    registry.fedoraproject.org/fedora:33
```

## Enter container with root
初期設定のためrootでコンテナに入る。
```sh
podman start fedora
podman exec -it fedora bash
```

## Initial settings
```sh
dnf install passwd
passwd $USER
usermod -aG wheel $USER
usermod -d /home/$USER $USER
usermod -s /bin/bash $USER
```
upstreamのアップデートとかで面倒なので特にイメージのビルドとかはしない（予定）。

## Enter container with user
```sh
podman exec -it -u $USER fedora bash
```
普段使いでは`$USER`を使う。podmanの`userns=keep-id`のおかげでボリューム内をホストからもコンテナからも同ユーザとして編集できる。
これでGUIアプリケーションの開発/テストがコンテナ上で簡単にできるはず。
