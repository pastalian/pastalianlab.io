---
title: "プロセスのネットワークを遮断"
date: 2020-03-29T21:20:53+09:00
description: nftablesとsgでネットワークを遮断
tags:
- network
categories:
- linux
---

専用グループを作成し、nftablesでそのグループのoutputをdrop、sgでグループ指定のプロセス起動をすることでネットワークの遮断をする。

## group
専用グループの作成。
```sh
groupadd no-internet
usermod -aG no-internet $USER
```

## nftables
`meta skgid`でgidでのマッチングができる。
作成したグループを指定してoutputをすべてdropする。

```sh
#!/sbin/nft -f
...
table inet filter {
	...
	chain output {
		type filter hook output priority 0; policy accept;
		meta skgid no-internet drop;
	}
}
```

## sg
適当にスクリプトかエイリアスを作ってプロセスを起動する。
```sh
sg no-internet "$*"
```
