---
title: "Raspberry Piのヘッドレス設定"
date: 2020-03-10T16:58:43+09:00
description: 大量のRPiを設定するはめになった時の記録
tags:
- rpi
categories:
- linux
---

研究でRaspberry Piを50台くらいセットアップしたのでその時の記録。

初めは4台くらいだったので手動でやっていたけど50台ともなるとキレるのでスクリプトを書いた。

```sh
#!/bin/sh

set -e

rpi_conf() {
    if [ $# -ne 2 ]; then
        echo "Set host index" 1>&2
        exit 1
    fi

    ID=`printf %02d $1`
    DID=$2
    MOUNT_DIR=/media/iso

    sudo mount /dev/sd${DID}1 ${MOUNT_DIR}
    sudo touch ${MOUNT_DIR}/ssh
    sudo cp wpa_supplicant.conf ${MOUNT_DIR}
    sudo umount ${MOUNT_DIR}
    
    sudo mount /dev/sd${DID}2 ${MOUNT_DIR}
    sudo cp dhcpcd.conf ${MOUNT_DIR}/etc/
    echo "IP: 192.168.2.2${ID}"
    echo "HOSTNAME: rpi4-${ID}"
    sudo sed -i -e "s/192.168.2.101/192.168.2.2${ID}/" ${MOUNT_DIR}/etc/dhcpcd.conf
    sudo sed -i -e "s/raspberrypi/rpi4-${ID}/" ${MOUNT_DIR}/etc/hostname
    sudo sed -i -e "s/raspberrypi/rpi4-${ID}/" ${MOUNT_DIR}/etc/hosts
    sudo umount ${MOUNT_DIR}
}

echo "BURNING!!!"
sudo dd bs=4M if=2020-02-13-raspbian-buster-lite.img of=/dev/sda conv=fsync &
sudo dd bs=4M if=2020-02-13-raspbian-buster-lite.img of=/dev/sdb conv=fsync &
sudo dd bs=4M if=2020-02-13-raspbian-buster-lite.img of=/dev/sdc conv=fsync &
sudo dd bs=4M if=2020-02-13-raspbian-buster-lite.img of=/dev/sdd conv=fsync &
sudo dd bs=4M if=2020-02-13-raspbian-buster-lite.img of=/dev/sde conv=fsync &
sudo dd bs=4M if=2020-02-13-raspbian-buster-lite.img of=/dev/sdf conv=fsync &
sudo dd bs=4M if=2020-02-13-raspbian-buster-lite.img of=/dev/sdg conv=fsync &
sudo dd bs=4M if=2020-02-13-raspbian-buster-lite.img of=/dev/sdh conv=fsync

wait && sync

IDX=$1

echo "CONFIGUREING!!!"
rpi_conf $IDX a
rpi_conf $(($IDX+1)) b
rpi_conf $(($IDX+2)) c
rpi_conf $(($IDX+3)) d
rpi_conf $(($IDX+4)) e
rpi_conf $(($IDX+5)) f
rpi_conf $(($IDX+6)) g
rpi_conf $(($IDX+7)) h

```

## やっていること
- `dd`でOSを`/dev/sd{a-h}`に書き込み
- それぞれに対し
    - sshの有効化
    - `wpa_supplicant.conf`のコピー
    - `dhcpcd.conf`のコピー
    - 固定IP設定
    - ホスト名変更
