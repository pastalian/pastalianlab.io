---
title: "Gentoo Tips"
date: 2020-03-29T07:30:15+09:00
description: Useful gentoo tips... maybe
tags:
- gentoo
categories:
- linux
---

Useful gentoo tips... maybe

## q applets

Portage関連の操作はできるだけこれを使いたい。他と比べて超早い。

- `qdepends -Qv <pkgname>`
    pkgの逆依存関係を取得
- `quse -DIvp <pkgname>`
    pkgのUSE flagを取得（未インストールのpkgを呼ぶ際は`I`フラグを外す）
- `qlist -Iv <sub-pkgname>`
    インストール済みのpkgを検索（部分一致検索）
- `qsearch -v <regx>`
    Portageからpkgを検索（`emerge -s`の実質上位互換）

## Fontconfig
自分で`/usr/share/fonts/`にフォントを突っ込むとxcb?が関係するビルドの際に下のエラーが発生することがある。

```
 * ACCESS DENIED:  mkostemp:     /usr/share/fonts/*/.uuid.TMP-XXXXXX
-- Font not found: fixed:pixelsize=10
 * ACCESS DENIED:  mkostemp:     /usr/share/fonts/*/.uuid.TMP-XXXXXX
-- Font not found: unifont:fontformat=truetype
 * ACCESS DENIED:  mkostemp:     /usr/share/fonts/*/.uuid.TMP-XXXXXX
 ```

フォントを自分で突っ込んだ後にはrootでキャッシュを再生成することが必要。
 ```sh
 fc-cache -f
 ```

## LXD
systemdベースのコンテナを使う際に処理が必要（ホストがOpenRCなので）。
```sh
mkdir -p /sys/fs/cgroup/systemd
mount -t cgroup -o none,name=systemd systemd /sys/fs/cgroup/systemd
```
sysfsはインメモリなのでホストを再起動するたびに生成したディレクトリは消える。

## Mozc
日本語入力環境は自分でパッケージを作ったりいろいろ試行錯誤したが、やっぱり公式レポジトリのメンテナンスフリーな点が魅力的過ぎた。
かと言って標準辞書はひどいので`NElogd UT`辞書をuser patchとして追加。

1. [辞書](http://linuxplayers.g1.xrea.com/mozc-neologd-ut.html)をダウンロードしてからオリジナルとdiffを取る
2. `/etc/portage/patches/app-i18n/mozc/`にpatchを置く
