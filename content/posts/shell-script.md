---
title: "Shell Script Tips"
date: 2020-04-10T15:33:00+09:00
description: Shell script tips
tags:
- shell
categories:
- linux
---

TIL（明日忘れること）的なshell scriptを追記していく。

## xargs
list系の出力をgrepでパターンマッチングしてからxargsで処理する。

```sh
# Remove all docker containers except "excluded".
docker ps -aq | grep -v "excluded" | xargs docker rm

# Remove all files in the current directory except "excluded".
find . -not -name excluded -print0 | xargs -0 rm
```

## Diff
```sh
# Sort then diff
## fish
diff (sort file1.txt | psub) (sort file2.txt | psub)
## bash
diff <(sort file1.txt) <(sort file2.txt)
```
