---
title: "Qutebrowser tips"
date: 2020-03-01T00:32:16+09:00
description: QutebrowserでQtのバージョンが反映されなかったので対処
tags:
- browser
categories:
- software
---

## 問題
`colors.webpage.prefers_color_scheme_dark`を試したかったのでQtのバージョンを5.14.1に上げたが5.14じゃないと怒られる。

#### TLDR
コアパッケージアップデートしたときは依存パッケージ全部ビルドしなおせ。

## 環境
Gentoo Linux
```
qutebrowser v1.10.1
Git commit: 
Backend: QtWebEngine (Chromium 77.0.3865.129)
Qt: 5.14.1 (compiled 5.13.2)
```

## 対処
とりあえずqutebrowserのソースを見る。
`version_check`という関数が定義されていて、PyQtから
- qVersion()
- QT\_VERSION\_STR
- PYQT\_VERSION\_STR

を持ってきてバージョンチェックしていた。
簡単だったので自分で確認してみる。

```python
from PyQt5.QtCore import (qVersion, QT_VERSION_STR, PYQT_VERSION_STR)

print("qVersion {}".format(qVersion()))
print("QT_VERSION_STR {}".format(QT_VERSION_STR))
print("PYQT_VERSION_STR {}".format(PYQT_VERSION_STR))
```
```
qVersion 5.14.1
QT_VERSION_STR 5.13.2
PYQT_VERSION_STR 5.14.1
```

`QT_VERSION_STR`を調べると
> This macro expands to a string that specifies Qt's version number (for example, "4.1.2"). This is the version against which the application is compiled.

PyQtが5.13.2でコンパイルされていたのでリビルド。

## 結論
Gentooならではのミスプレイ。コアパッケージをアップデートしたときは依存パッケージをちゃんとリビルドしよう。（qtcore, qtwebengine, PyQt5同時にアップデートした気がするのでなぜビルド順番がこうなったのか不明。portageはもっと賢いと思っていた...）
