---
title: "Podman Pod"
date: 2020-11-07T02:13:16+09:00
description: Minifluxをpodで立ててみる
tags:
- container
categories:
- dev
---

## 背景
[Miniflux](https://miniflux.app/)をコンテナで試してみようと思ったらドキュメントにdocker-composeの例が載ってた
-> [podman-compose](https://github.com/containers/podman-compose)は現状ゴミ

## Pods
podmanではdocker-compose（つまりpodman-compose）を使うよりもKubernetesのPodsとかいうやつが推奨されているらしい。
Kubernetesを使ったことがないのでPodsの詳細については今回なかったことにして、とりあえずMinifluxを例にしてコマンドを列挙する。

### Create pod
ホストへのexposeはpod側で指定しておく。
```sh
podman pod create --name miniflux-pod -p 8080
```

### Create connected container
```sh
podman run -d \
    --pod miniflux-pod \
    -e POSTGRES_USER=miniflux \
    -e POSTGRES_PASSWORD=secret \
    --name miniflux-db \
    postgres

podman run -d \
    --pod miniflux-pod \
    -e DATABASE_URL="postgres://miniflux:secret@localhost/miniflux?sslmode=disable" \
    -e RUN_MIGRATIONS=1 \
    -e CREATE_ADMIN=1 \
    -e ADMIN_USERNAME=admin \
    -e ADMIN_PASSWORD=test123 \
    --name miniflux \
    miniflux/miniflux
```
pod内にコンテナを作ることでデータベースにlocalhostで接続できる。

## 謎
Minifluxのドキュメントにmigrationを`docker exec`でやる方法が書いてある。
```sh
# Run database migrations
docker-compose exec miniflux /usr/bin/miniflux -migrate

# Create the first user
docker-compose exec miniflux /usr/bin/miniflux -create-admin
```

migration前にminifluxを立ち上げるとすぐにexitするので普通にexecできない。
結果超面倒な方法しか思いつかなかった。

```sh
podman run -it \
    --pod miniflux-pod \
    -e DATABASE_URL="postgres://miniflux:secret@localhost/miniflux?sslmode=disable" \
    --name miniflux-app \
    miniflux/miniflux /bin/sh -c "/usr/bin/miniflux -migrate; /usr/bin/miniflux -create-admin"

podman commit miniflux-app miniflux-local

podman run -d \
    --pod miniflux-pod \
    --name miniflux-app2 \
    miniflux-local /usr/bin/miniflux
```

これは酷い。

## Reference
[Installation Instructions - Documentation](https://miniflux.app/docs/installation.html#docker)
[Moving from docker-compose to Podman pods | Enable Sysadmin](https://www.redhat.com/sysadmin/compose-podman-pods)
