---
title: "Gentoo PR"
date: 2021-01-06T23:59:26+09:00
description: GentooのPortage TreeにGitHubからPRを送る
tags:
- gentoo
categories:
- dev
---

基本[ここ](https://wiki.gentoo.org/wiki/GitHub_Pull_Requests)に従えば大丈夫なはずだけど日本語でもメモしておく。

## GPG鍵生成
Gentooにcontributeするためには[Certificate of Origin](https://www.gentoo.org/glep/glep-0076.html#certificate-of-origin)に同意する必要があり、そのためにコミットメッセージに
`Signed-off-by: Name <e-mail>`
という署名を含める。当たり前だがテキストを含めるだけなら誰でもできてしまうのでGPG鍵でcommit自体も署名する。
肝心の作成方法は[GLEP 63](https://wiki.gentoo.org/wiki/Project:Infrastructure/Generating_GLEP_63_based_OpenPGP_keys)に従えば安心。これはdeveloper用のガイドなのであれだけど参考程度に（そもそも@gentoo.orgのアドレスじゃないので）。
ちなみに署名関係のNameは全て`legal name as a natural person`なのでニックネーム等使用しないよう注意。

## Githubからフォーク
ほぼコピペでいいので略
[Repository's remote configuration](https://wiki.gentoo.org/wiki/GitHub_Pull_Requests#Repository.27s_remote_configuration)

## Gitの設定
同じくコピペ
[Repository's user configuration](https://wiki.gentoo.org/wiki/GitHub_Pull_Requests#Repository.27s_user_configuration)


## GPGの設定
コピペ
[GPG configuration](https://wiki.gentoo.org/wiki/GitHub_Pull_Requests#GPG_configuration)

## 追加設定
現時点でwikiに書いてないけど、後で使う`repoman`のために`/etc/portage/make.conf`に
`SIGNED_OFF_BY="Name <e-mail>"`
を追加しておかないとコミットメッセージへの署名の追加が自動でされなかった。

## コミット
変更のステージ後にQAのためチェックを`repoman`で行う
```
repoman manifest
repoman -dx full
pkgcheck scan
```
とくに問題なさそうだったら
`repoman ci` で署名とかを自動でやってcommitを作成できる。
あとはフォークにpushしてPRを作成。developerが反応してくれるのを待つだけ。

![img](/images/21-01-07_011112.png)

...メンテナが死んでないpackageへのPRならすぐに反応してくれるはず。
