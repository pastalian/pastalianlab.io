---
title: "Traefik"
date: 2020-04-17T22:17:37+09:00
description: traefikを試す
tags:
- container
- network
categories:
- dev
---

traefikを試す

## Background
nginxでセルフホスティングのDockerサービスを管理するのが辛く感じ始めたのでユーザフレンドリーっぽい見た目のtraefikが気になった。

## Dashboard only
技術的概要はドキュメントを読めばわかるので、とりあえずダッシュボードのみをdockerでセットアップしてみる。

```docker-compose.yml
version: '3'

services:
  traefik:
    image: traefik:v2.2
    ports:
      - 80:80
    command:
      - --entrypoints.http.address=:80
      - --providers.docker.exposedByDefault=false
      - --api=true
    labels:
      traefik.enable: 'true'
      traefik.http.routers.dashboard.rule: Host(`traefik.local`)
      traefik.http.routers.dashboard.entrypoints: http
      traefik.http.routers.dashboard.service: api@internal
      traefik.http.routers.dashboard.middlewares: auth
      traefik.http.middlewares.auth.basicauth.users: test:$$apr1$$h6auq4f2$$wPi.NHbi5mk0HRu7NtyGp.
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock

```

`command`部分はstatic configurationでtraefikの実行時に設定される。
`labels`部分はdynamic configurationでtraefikの実行中に設定できる（この例ではtraefik自体に設定しているのでどちらにしろリスタートが必要）。

authではhtpasswdを使用してuser,passwordを設定する。
> ymlでは`$`をエスケープしなければならないので注意

![dashboard](/images/20-04-18_010550.png)

## Add services
試しにnginxを追加してみる。

```docker-compose.yml
...
  nginx:
    image: nginx:alpine
    labels:
      traefik.enable: 'true'
      traefik.http.services.nginx.loadbalancer.server.port: 80
      traefik.http.routers.nginx.rule: Host(`nginx.local`)
      traefik.http.routers.nginx.entrypoints: http
```

## Enable https
httpをhttpsにリダイレクトさせるミドルウェアを追加する。

```docker-compose.yml
version: '3'

services:
  traefik:
    image: traefik:v2.2
    ports:
      - 80:80
      - 443:443
    command:
      - --entrypoints.http.address=:80
      - --entrypoints.https.address=:443
      - --providers.docker.exposedByDefault=false
      - --api=true
    labels:
      traefik.enable: 'true'
      traefik.http.routers.dashboard-http.rule: Host(`traefik.local`)
      traefik.http.routers.dashboard-http.entrypoints: http
      traefik.http.routers.dashboard-http.middlewares: dashboard-redirect
      traefik.http.middlewares.dashboard-redirect.redirectscheme.scheme: https
      traefik.http.routers.dashboard.rule: Host(`traefik.local`)
      traefik.http.routers.dashboard.entrypoints: https
      traefik.http.routers.dashboard.tls: 'true'
      traefik.http.routers.dashboard.service: api@internal
      traefik.http.routers.dashboard.middlewares: auth
      traefik.http.middlewares.auth.basicauth.users: test:$$apr1$$h6auq4f2$$wPi.NHbi5mk0HRu7NtyGp.

    volumes:
      - /var/run/docker.sock:/var/run/docker.sock

  nginx:
    image: nginx:alpine
    labels:
      traefik.enable: 'true'
      traefik.http.services.nginx.loadbalancer.server.port: 80
      traefik.http.routers.nginx-http.rule: Host(`nginx.local`)
      traefik.http.routers.nginx-http.entrypoints: http
      traefik.http.routers.nginx-http.middlewares: nginx-redirect
      traefik.http.middlewares.nginx-redirect.redirectscheme.scheme: https
      traefik.http.routers.nginx.rule: Host(`nginx.local`)
      traefik.http.routers.nginx.entrypoints: https
      traefik.http.routers.nginx.tls: 'true'
```

## Add Let's Encrypt support
testなのでstaging環境にリクエストを送信する。

```docker-compose.yml
version: '3'

services:
  traefik:
    ...
    command:
      ...
      - --certificatesResolvers.letsencrypt_resolver.acme.email=example@example.com
      - --certificatesResolvers.letsencrypt_resolver.acme.storage=acme.json
      - --certificatesResolvers.letsencrypt_resolver.acme.httpChallenge.entryPoint=http
      - --certificatesResolvers.letsencrypt_resolver.acme.caServer=https://acme-staging-v02.api.letsencrypt.org/directory # test environment
    labels:
      ...
      traefik.http.routers.dashboard.tls: 'true'
      traefik.http.routers.dashboard.tls.certresolver: letsencrypt_resolver

  nginx:
    ...
    labels:
      ...
      traefik.http.routers.nginx.tls: 'true'
      traefik.http.routers.nginx.tls.certresolver: letsencrypt_resolver
```

ページを表示して`Fake LE Intermediate X1`が発行されていればテストは成功。

![certificates](/images/20-04-18_023341.png)

## Then...
調べてる最中に [nginx proxy manager](https://github.com/jc21/nginx-proxy-manager) というのを見つけてこっちの方が良さそうに感じた。
これも試して良さそうな方で実環境を管理したい。

## Reference
https://baptiste.bouchereau.pro/tutorial/configuration-differences-between-traefik-v1-and-v2-with-the-docker-provider/
