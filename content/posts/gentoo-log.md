---
title: "Gentoo Log"
date: 2019-12-11T21:42:36+09:00
description: GentooをDell Optiplex 7070にインストールした際の記録
tags:
- gentoo
categories:
- linux
---

GentooをDell Optiplex 7070にインストールした際の記録。

# 前準備
Windowsとのデュアルブートにするため先にWin10のセットアップを済ませる。
- 高速スタートアップの無効化
- BIOSでセキュアブートの無効化
- BIOSでSATAコントローラー設定をRAIDからAHCIに変更
    - この作業には罠があり、
        1. Windowsの次回起動をセーフモードに設定 (win-r => msconfig => boot => safe boot)
        2. BIOSでAHCIに変更
        3. Windowsをセーフモードで起動 (その後同手順でセーフモード無効化)

        の手順を踏まなければWindowsがブルースクリーンを出してくる。

# システムインストール
素直に [公式ハンドブック](https://wiki.gentoo.org/wiki/Handbook:AMD64) に従う。
kernelは面倒だったのでgenkernelで済ませたが、起動速度が気に入らないので時間のある時にしっかり設定したい。

# その他インストール
### alsa
ヘッドホン接続した際に音が出ないことが判明。マイクと混同した3.5mジャックなのでこいつのせいだろうと調べたら案の定だった。
`/etc/modprobe.d/alsa.conf`に`options snd-hda-intel model=headset-mic`を追記で解決。
modelの種類がいくつかあり、初めは`dell-headset-multi`を選択していたが起動時に音が出たり出なかったり不安定だったのでやめた。

### git
gitが依存するperlのモジュールをビルドする際にエラーが発生。
システムインストール後にシステムクロックを変えたため、
cmake(?)あたりがperlのファイルが現時刻より先だとかいう意味不明な文句を言ってくるのが原因だった。
perlをリビルドして解決。

### neovim
依存パッケージのmsgpackのビルドがエラーを吐く。ninjaの100回リトライ機能が邪魔すぎてログを読む気が失せたため中断。
翌日試したら成功した。原因不明。
