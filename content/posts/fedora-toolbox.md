---
title: "Fedora Toolbox"
date: 2020-10-25T03:41:36+09:00
description: Unprivileged development environment
tags:
- container
categories:
- dev
---

## What is this
https://github.com/containers/toolbox
ホストをそのまま使用しているかのような使い勝手のコンテナを作成し、開発環境として活用できる。

バックエンドにはpodmanが使用されていて、`toolbox create` とするだけでホストとシームレスに統合されたコンテナが作成される。
が、実行時依存にsystemdがあるためnon systemdのGentooでは使用できず非常に残念。

VM上の[Silverblue](https://silverblue.fedoraproject.org/)[^1]上で軽く試したところ、ほんとにホストにいるような感覚で開発環境を構築でき、GUIアプリまでホストに表示できた。現状VSCodeのホストからのコンテナ接続が動かないのが残念だけど対応策は一応あるのでなんとかなる ([#3345](https://github.com/microsoft/vscode-remote-release/issues/3345))。

[^1]: Immutable OS, 開発PCとして実用レベルなのでFedora33のリリース後にラップトップのOSにしてみようと思う。

## So
環境を汚すことに精神的苦痛を感じる自分にとっては非常に嬉しいツール。
systemdに依存する限りメインpcにやってくることはないのでコードを少し見てなんとかしてみたい。
まだシェルスクリプトのラッパー部分しか見ていなくてgoで何をしているのかは検討もつかない...
