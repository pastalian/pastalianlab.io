+++
title = "Bert with Jumanpp"
date = "2019-10-17"
tags = ["nlp", "python"]
categories = ["dev"]
description = "ColabでBERTを試してみる Part2"
+++

ColabでBERTを試してみる Part2

BERTの多言語モデルだと精度があまり良くなかったので今回は
[BERT日本語Pretrainedモデル](http://nlp.ist.i.kyoto-u.ac.jp/index.php?BERT%E6%97%A5%E6%9C%AC%E8%AA%9EPretrained%E3%83%A2%E3%83%87%E3%83%AB)
を使用する

## 1. Mount Google Drive
いつもの
```
from google.colab import drive
drive.mount('/content/drive')
```

## 2. Build JUMAN++ and download the pretrained model (first time only)
今回使用するモデルはトークナイズにJUMAN++を使用するため、Colab上でビルドする。
また、毎回ビルドするのは時間がかかるのでドライブにコピーしておく。
モデルのダウンロードも重いので同様。
```shell
!curl -OL https://github.com/ku-nlp/jumanpp/releases/download/v2.0.0-rc2/jumanpp-2.0.0-rc2.tar.xz
!tar xf jumanpp-2.0.0-rc2.tar.xz
%cd jumanpp-2.0.0-rc2
!mkdir bld
%cd bld
!cmake .. -DCMAKE_BUILD_TYPE=Release
!make install -j8

!mkdir /content/drive/My\ Drive/bin /content/drive/My\ Drive/libexec
!cp /usr/local/bin/jumanpp /content/drive/My\ Drive/bin/
!cp /usr/local/libexec/jumanpp /content/drive/My\ Drive/libexec -r

!wget 'http://nlp.ist.i.kyoto-u.ac.jp/DLcounter/lime.cgi?down=http://nlp.ist.i.kyoto-u.ac.jp/nl-resource/JapaneseBertPretrainedModel/Japanese_L-12_H-768_A-12_E-30_BPE.zip&name=Japanese_L-12_H-768_A-12_E-30_BPE.zip'
!unzip 'lime.cgi?down=http:%2F%2Fnlp.ist.i.kyoto-u.ac.jp%2Fnl-resource%2FJapaneseBertPretrainedModel%2FJapanese_L-12_H-768_A-12_E-30_BPE.zip&name=Japanese_L-12_H-768_A-12_E-30_BPE.zip'
!cp ./Japanese_L-12_H-768_A-12_E-30_BPE /content/drive/My\ Drive/bert/ -r
```

## 3. Copy the binary
初回以降は2.のファイルをドライブからコピーするだけ
```shell
!cp /content/drive/My\ Drive/bin/jumanpp /usr/local/bin
!mkdir -p /usr/local/libexec
!cp -r /content/drive/My\ Drive/libexec/jumanpp /usr/local/libexec/
!chmod +x /usr/local/bin/jumanpp \
  /usr/local/libexec/jumanpp/jumandic.config \
  /usr/local/libexec/jumanpp/jumandic.jppmdl
```

## 4. Clone BERT
トークナイズにJUMAN++を使用するようにしたBERTをcloneする。
```python
!git clone -q https://gitlab.com/pastalian/bert.git
%cd bert
```

## 5. Install requrements
```shell
!pip install -qr requirements.txt
```

## 6. Set env values
各種変数を設定する。
4.のBERTはラベルを`NUM_LABELS`で指定するようにしたのでいい感じにする。`labels = [0..NUM_LABELS-1]`
```shell
BERT_BASE_DIR='/content/drive/My\ Drive/bert/Japanese_L-12_H-768_A-12_E-30_BPE'
DATA_DIR='/content/drive/My\ Drive/data'
OUTPUT_DIR='/content/bert/model'
EXE_DIR='/content/bert'
%env NUM_LABELS=3
```

## 7. Fine-tuning
オリジナルから変更した箇所はtaskのみなので公式READMEを見て雰囲気設定。
`max_seq_length`を256にしたかったがメモリが足りないので断念。
```shell
%%time
!python {EXE_DIR}/run_classifier.py \
  --task_name=my \
  --do_train=true \
  --do_eval=true \
  --data_dir={DATA_DIR} \
  --vocab_file={BERT_BASE_DIR}/vocab.txt \
  --bert_config_file={BERT_BASE_DIR}/bert_config.json \
  --init_checkpoint={BERT_BASE_DIR}/bert_model.ckpt \
  --max_seq_length=128 \
  --train_batch_size=32 \
  --learning_rate=2e-5 \
  --num_train_epochs=3.0 \
  --output_dir={OUTPUT_DIR}
```

## 8. Prediction
7.と同様。
`init_checkpoints`の`XX`は7.の出力を見て設定する。
結果は`{OUTPUT_DIR}/test_results.tsv`に出力される。
```shell
!python {EXE_DIR}/run_classifier.py \
  --task_name=my \
  --do_predict=true \
  --data_dir={DATA_DIR} \
  --vocab_file={BERT_BASE_DIR}/vocab.txt \
  --bert_config_file={BERT_BASE_DIR}/bert_config.json \
  --init_checkpoint={OUTPUT_DIR}/model.ckpt-XX \
  --max_seq_length=128 \
  --train_batch_size=32 \
  --learning_rate=2e-5 \
  --num_train_epochs=3.0 \
  --output_dir={OUTPUT_DIR}
```

## 9. Show result
結果をargmaxで正解ラベルと比較するだけのプログラム。
```shell
!python {EXE_DIR}/compute_accuracy.py --data {DATA_DIR} --result {OUTPUT_DIR}
```

## Conclusion
2値分類だと自分で集めた適当な文章でも精度が高いので脳死できる。
ただしラベルを増やしていくと当たり前のように精度が下がっていくので、自然言語処理の基礎知識がないと辛い。（ないので終わり）

